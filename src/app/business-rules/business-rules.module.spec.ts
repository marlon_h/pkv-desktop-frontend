import { BusinessRulesModule } from './business-rules.module';

describe('BusinessRulesModule', () => {
  let businessRulesModule: BusinessRulesModule;

  beforeEach(() => {
    businessRulesModule = new BusinessRulesModule();
  });

  it('should create an instance', () => {
    expect(businessRulesModule).toBeTruthy();
  });
});
