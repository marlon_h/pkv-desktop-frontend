export interface TariffItem {
    id: number;
    name: string;
    price: number;
}
