import { Component } from '@angular/core';

@Component({
  selector: 'pkv-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {
  title = 'pkv-desktop-frontend';
}
