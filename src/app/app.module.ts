import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ResultPageModule } from './result-page/result-page.module';
import { AppRoutingModule } from './app-routing.module';
import { ResultListModule } from './result-page/result-list/result-list.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    ResultPageModule,
    ResultListModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
