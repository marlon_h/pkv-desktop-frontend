import { FilterBase } from './filter-base';

export class CheckboxFilter extends FilterBase<boolean> {
    controlType = 'checkbox';

    constructor(options: {} = {}) {
        super(options);
    }
}
