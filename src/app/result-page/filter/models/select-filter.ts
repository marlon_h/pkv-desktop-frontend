import { FilterBase } from './filter-base';

export class SelectFilter extends FilterBase<string> {
    controlType = 'select';
    options: { key: string, value: string }[] = [];

    constructor(options: {} = {}) {
        super(options);
        this.options = options['options'] || [];
    }
}
