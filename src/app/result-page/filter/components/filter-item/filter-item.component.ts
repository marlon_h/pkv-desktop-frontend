import { Component, Input } from '@angular/core';
import { FilterBase } from '../../models/filter-base';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'pkv-filter-item',
  templateUrl: './filter-item.component.html',
  styleUrls: ['./filter-item.component.less']
})
export class FilterItemComponent {
  @Input() filter: FilterBase<any>;
  @Input() form: FormGroup;
}
