import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { FilterControlService } from './services/filter-control.service';
import { FormGroup } from '@angular/forms';
import { FilterBase } from './models/filter-base';
import { FilterService } from './services/filter.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'pkv-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.less'],
  providers: [ FilterControlService, FilterService ]
})
export class FilterComponent implements OnInit, OnDestroy {
  @Output() updateEvent = new EventEmitter<any>();

  form: FormGroup;
  filters: FilterBase<any>[] = [];
  filterSubscription: Subscription;

  constructor(private filterControlService: FilterControlService, private filterService: FilterService) { }

  ngOnInit() {
    this.filters = this.filterService.getFilters();
    this.form = this.filterControlService.toFormGroup(this.filters);
    this.filterSubscription = this.form.valueChanges.subscribe(
        values => this.updateEvent.emit(values));
  }

  ngOnDestroy() {
    this.filterSubscription.unsubscribe();
  }
}
