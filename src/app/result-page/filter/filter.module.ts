import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FilterComponent } from './filter.component';
import { FilterItemComponent } from './components/filter-item/filter-item.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  exports: [FilterComponent],
  declarations: [FilterComponent, FilterItemComponent]
})
export class FilterModule { }
