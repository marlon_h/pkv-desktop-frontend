import { Injectable } from '@angular/core';

import { FilterBase } from '../models/filter-base';
import { CheckboxFilter } from '../models/checkbox-filter';
import { SelectFilter } from '../models/select-filter';

@Injectable()
export class FilterService {
    getFilters() {
        const filters: FilterBase<any>[] = [
            new CheckboxFilter({
                key: 'filter1',
                label: 'Filter 1',
            }),
            new CheckboxFilter({
                key: 'filter2',
                label: 'Filter 2',
            }),
            new CheckboxFilter({
                key: 'filter3',
                label: 'Filter 3',
            }),
            new SelectFilter({
                key: 'filter4',
                label: 'Filter 4',
                value: 'option_2',
                options: [
                    {
                        value: 'Option 1',
                        key: 'option_1'
                    },
                    {
                        value: 'Option 2',
                        key: 'option_2'
                    },
                    {
                        value: 'Option 3',
                        key: 'option_3'
                    },
                ]
            })
        ];

        return filters;
    }
}
