import { Injectable } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

import { FilterBase } from '../models/filter-base';

@Injectable()
export class FilterControlService {
    constructor() {}

    toFormGroup(filters: FilterBase<any>[]) {
        const group: any = {};

        filters.forEach(filter => {
            group[filter.key] = new FormControl(filter.value || '');
        });

        return new FormGroup(group);
    }
}
