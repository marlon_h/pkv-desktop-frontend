import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ResultPageComponent } from './result-page.component';

const routes: Routes = [
    {
      path: 'vergleichsergebnis',
      component: ResultPageComponent
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ResultPageRoutingModule { }
