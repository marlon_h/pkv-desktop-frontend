import { Component, Input, OnInit } from '@angular/core';
import { TariffItem } from '../../../../business-rules/interfaces/tariff-item';

@Component({
  selector: 'pkv-tariff-item',
  templateUrl: './tariff-item.component.html',
  styleUrls: ['./tariff-item.component.less']
})
export class TariffItemComponent implements OnInit {
  @Input() tariffItem: TariffItem;

  constructor() { }

  ngOnInit() {
  }

}
