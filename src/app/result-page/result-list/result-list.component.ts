import { Component, Input } from '@angular/core';
import { TariffItem } from '../../business-rules/interfaces/tariff-item';

@Component({
  selector: 'pkv-result-list',
  templateUrl: './result-list.component.html',
  styleUrls: ['./result-list.component.less']
})
export class ResultListComponent {
  @Input() result: TariffItem[];
}
