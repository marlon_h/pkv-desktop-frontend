import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ResultListComponent } from './result-list.component';
import { TariffItemComponent } from './components/tariff-item/tariff-item.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [ResultListComponent, TariffItemComponent],
  exports: [ResultListComponent]
})
export class ResultListModule { }
