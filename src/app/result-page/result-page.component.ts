import {Component, OnInit} from '@angular/core';
import {TariffItem} from '../business-rules/interfaces/tariff-item';

@Component({
    selector: 'pkv-result-page',
    templateUrl: './result-page.component.html',
    styleUrls: ['./result-page.component.less']
})
export class ResultPageComponent implements OnInit {
    result: TariffItem[];

    ngOnInit() {
        this.result = [
            {
                id: 1,
                name: 'Foo',
                price: 340.40
            },
            {
                id: 2,
                name: 'Bar',
                price: 240.40
            },
            {
                id: 3,
                name: 'Foobar',
                price: 640.40
            },
            {
                id: 4,
                name: 'Barfoo',
                price: 140.40
            },
            {
                id: 5,
                name: 'FooFoo',
                price: 840.40
            }
        ];
    }

    onFilterUpdate(event) {
        console.log(event);
    }
}
