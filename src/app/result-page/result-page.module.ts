import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ResultPageRoutingModule } from './result-page-routing.module';
import { ResultPageComponent } from './result-page.component';
import { FilterModule } from './filter/filter.module';
import { ResultListModule } from './result-list/result-list.module';

@NgModule({
  imports: [
    CommonModule,
    ResultPageRoutingModule,
    FilterModule,
    ResultListModule,
  ],
  declarations: [ResultPageComponent],
  exports: [ResultPageComponent]
})
export class ResultPageModule { }
